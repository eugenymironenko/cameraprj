package com.test.cameraprj

import android.os.Handler
import android.os.HandlerThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

class HandlerThreadScheduler(name: String) : Scheduler() {
    private val handlerThread = HandlerThread(name)

    init {
        handlerThread.start()
    }

    val handler = Handler(handlerThread.looper)

    override fun createWorker(): Worker {
        return AndroidSchedulers.from(handlerThread.looper).createWorker()
    }
}