package com.test.cameraprj

import android.util.Rational
import android.util.Size
import android.view.Surface
import androidx.camera.core.*
import androidx.lifecycle.LifecycleOwner
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.nio.ByteBuffer

class CameraXSdk(private val lifecycleOwner: LifecycleOwner) {

    private var imageAnalysis: ImageAnalysis? = null

    private val cameraRunningRelay = BehaviorRelay.create<Boolean>()

    private val dataObservable = Observable.create<CameraFrameData> { emitter ->
        imageAnalysis?.setAnalyzer { image, rotationDegrees ->
            image.image?.let {
                val frameData = CameraFrameData(image.planes[0].buffer, image.planes[1].buffer, image.planes[2].buffer, image.height, image.width)
                emitter.onNext(frameData)
            }
        }
        emitter.setCancellable {
            imageAnalysis?.removeAnalyzer()
        }
    }.subscribeOn(Schedulers.io()).share()

    fun createCameraDataObservable(): Observable<CameraFrameData> {
        return Completable.fromCallable {
            CameraX.unbindAll()
            imageAnalysis = ImageAnalysis(createImageAnalysisConfig())
            val imageCapture = ImageCapture(createImageCaptureConfig())

            val previewConfig  = createImagePreviewConfig();
            val preview = Preview(previewConfig)

            CameraX.bindToLifecycle(lifecycleOwner,imageAnalysis, preview)
            preview.enableTorch(true)
            cameraRunningRelay.accept(true)

        }.andThen(dataObservable)
    }

    private fun createImageAnalysisConfig() = ImageAnalysisConfig.Builder()
            .setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
            .setLensFacing(CameraX.LensFacing.BACK)
            .setTargetAspectRatio(Rational(4, 3))
            .setTargetResolution(Size(640, 480))
            .setImageQueueDepth(2)
            .setCallbackHandler(HandlerThreadScheduler("brain").handler)
            .build()

    private fun createImageCaptureConfig() = ImageCaptureConfig.Builder()
        .setFlashMode(FlashMode.ON)
        .build()

    private fun createImagePreviewConfig() = PreviewConfig.Builder()
        .setLensFacing(CameraX.LensFacing.BACK)
        .setTargetResolution(Size(640, 480))
        .setTargetRotation(Surface.ROTATION_270)
        .build()

    class CameraFrameData(val yBuffer: ByteBuffer, val uBuffer: ByteBuffer, val vBuffer: ByteBuffer, val frameHeight: Int, val frameWidth: Int)

}
