package com.test.cameraprj

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import java.nio.ByteBuffer

class MainActivity : AppCompatActivity() {

    private var initCamDisposable: Disposable? = null

    private val cameraXSdk = CameraXSdk(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        System.loadLibrary("native-lib")

        benchRes.text = "Running benchmark"
    }

    external fun stringFromJNI(): String

    external fun myStringFromCpp(): String

    external fun initDetector(video_fps: Int, frame_width: Int, frame_height: Int): Unit

    external fun testDetectorPerf(frames_num: Int, frame_width: Int, frame_height: Int): Double

    external fun uploadFrameToDetector(
        y_buffer: ByteBuffer,
        u_buffer: ByteBuffer,
        v_buffer: ByteBuffer,
        frame_width: Int,
        frame_height: Int
    ): Unit

    external fun getLastBpm(): Double

    override fun onStart() {
        super.onStart()
//        var a: Int = 1
//        val b: Int = 1

        benchRes.text = "\nFps is : " + String.format("%.2f", testDetectorPerf(100, 1920, 1080))

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            startCamera()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
        }
    }

    override fun onStop() {
        super.onStop()
        initCamDisposable?.dispose()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCamera()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun startCamera() {
        initCamDisposable = cameraXSdk.createCameraDataObservable().observeOn(AndroidSchedulers.mainThread()).subscribe { cameraFrame ->
            Log.d(TAG, "yBuffer = ${cameraFrame.yBuffer}\nuBuffer = ${cameraFrame.uBuffer}\nvBuffer = ${cameraFrame.vBuffer}\n")

            initDetector(30, cameraFrame.frameWidth, cameraFrame.frameHeight)
            uploadFrameToDetector(cameraFrame.yBuffer, cameraFrame.uBuffer, cameraFrame.vBuffer, cameraFrame.frameWidth, cameraFrame.frameHeight)
            //tvMessage.text = myStringFromCpp()
            tvMessage.text = "Working...\nCur bpm is " + String.format("%.2f", getLastBpm())
        }
    }

    companion object {
        private const val TAG = "MainActivity"

        const val REQUEST_CAMERA_PERMISSION = 13524
    }

}
