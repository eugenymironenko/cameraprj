//
// Created by Nikita Mishchanka on 2019-07-02.
//

#pragma once

#include <thread>
#include <vector>
#include <array>
#include <queue>
#include <algorithm>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <memory>
#include <numeric>

#include "signal_filter.hpp"

static constexpr size_t MAX_FRAME_SIZE = 1920 * 1080;
using pixel_type = uint8_t;
using ONE_COLOR_FRAME = std::array<pixel_type, MAX_FRAME_SIZE>;
using THREE_COLOR_FRAME = std::array<ONE_COLOR_FRAME, 3>;
using YUV_FRAME = std::array<pixel_type, 2 * MAX_FRAME_SIZE>;

class pulse_detector {
private:
    void thread_function();
    
    double count_pixel_avg(ONE_COLOR_FRAME* frame_ptr);
    
    double count_pixel_avg(shared_ptr<THREE_COLOR_FRAME>& frame_ptr);

    double count_pixel_avg();

    std::vector<double> rolling_mean_window(const std::vector<double> &input, size_t window_size);

    std::vector<double> simple_signal_normalization(const std::vector<double> &input, size_t window_size = 30);

    std::vector<size_t> get_max_indices(const std::vector<double> &input, size_t window_size);

    std::vector<size_t>
    specify_max_indices(const std::vector<double> &input, const std::vector<size_t> &max_indices, size_t window_size);

    void calculate_bpm(const std::vector<size_t> &input);

    void convert_and_append_to_beats(const std::vector<size_t> &input);

    void calculate_hrv(const std::vector<double> &input, const bool &convert_to_ms = true);

    void filter_hrv_list_quantiles(const size_t &window_size = 50, const double &lower_quantile = 0.05,
                                   const double &upper_quantile = 0.95);

    void filter_hrv_list_median(const size_t &window_size = 50, const double &lower_mul = 0.7,
                                const double &upper_mul = 1.3);

    bool stop_execution = false;
    std::thread m_thread;

    static constexpr bool ENABLE_FRAME_CROP = true;
    static constexpr size_t AVG_WINDOW_SIZE = 5;
    const int CROP_WIDTH = 240, CROP_HEIGHT = 480;
    size_t frame_width = 720, frame_height = 1280;
        std::queue<shared_ptr<ONE_COLOR_FRAME>> frames_to_process;
    
    int producer_fps = 0;
    double window_duration_sec = 0;
    bool use_passband_filter;
    std::vector<double> processed_frames;
    std::vector<double> bpm_list;
    std::vector<double> beats_list;
    std::vector<double> hrv_list;

    double bpm_low = 40.0; // Lowest BPM 40 -> per second 40 / 60
    double bpm_high = 200.0; // Highest BPM 200 -> per second 200 / 60

public:
    pulse_detector(const int &video_fps, const double &window_duration = 10, const bool& use_filter = true);

    ~pulse_detector();
    
    void set_frame_size(const size_t& new_frame_width, const size_t& new_frame_height);

    void wait_processing_complete();

    void upload_rgb_frame(shared_ptr<THREE_COLOR_FRAME>& frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height);
    
    void instant_process_green_frame(shared_ptr<ONE_COLOR_FRAME>& frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height);
    
    void instant_process_rgb_frame(shared_ptr<THREE_COLOR_FRAME>& frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height);
    
    void instant_process_yuv_frame(YUV_FRAME* frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height);

    void flush_frames();

    std::vector<double> get_bpm() const;

    std::vector<double> get_beats() const;

    std::vector<double> get_hrv() const;

    double get_sdnn() const;

    double get_rmssd() const;

    double get_stress_level() const;

    void set_butterworth_freq(double low_freq, double hi_freq); // DEBUG METHOD
};
