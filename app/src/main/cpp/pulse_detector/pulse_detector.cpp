//
// Created by Nikita Mishchanka on 2019-07-04.
//

#include "pulse_detector.hpp"

void pulse_detector::flush_frames() {
    // process data!
    std::vector<double> normalized_frames = simple_signal_normalization(processed_frames);
    std::vector<double> filtered_signal;
    if (use_passband_filter) {
        double freq_low = 40.0 / 60; // Lowest BPM 40 -> per second 40 / 60
        double freq_high = 180.0 / 60; // Highest BPM 180 -> per second 180 / 60
        double fs = producer_fps;
        int filter_order = 8;
        static signal_filter noise_filter(freq_low, freq_high, fs, filter_order);
        filtered_signal = noise_filter.filter_signal(normalized_frames);
    }
    else {
        filtered_signal = normalized_frames;
    }
    std::vector<double> avg_input = rolling_mean_window(filtered_signal, AVG_WINDOW_SIZE);
    std::vector<size_t> max_ind = get_max_indices(avg_input, AVG_WINDOW_SIZE);
    std::vector<size_t> detailed_max_ind = specify_max_indices(normalized_frames, max_ind, AVG_WINDOW_SIZE);

    calculate_bpm(detailed_max_ind);
    convert_and_append_to_beats(detailed_max_ind);
    calculate_hrv(beats_list);
    filter_hrv_list_median();

    processed_frames.clear();
}

void pulse_detector::thread_function() {
    while (!stop_execution) {
        if (!frames_to_process.empty()) {
            processed_frames.push_back(count_pixel_avg());
        }

        if (processed_frames.size() >= producer_fps * window_duration_sec) {
            flush_frames();
        }
    }
}

pulse_detector::pulse_detector(const int &video_fps, const double &window_duration, const bool& use_filter) :
        producer_fps(video_fps),
        window_duration_sec(window_duration),
        use_passband_filter(use_filter) {
    processed_frames.reserve(producer_fps * window_duration_sec);
    m_thread = std::thread(&pulse_detector::thread_function, this);
}

pulse_detector::~pulse_detector() {
    while (!frames_to_process.empty()) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    stop_execution = true;
    m_thread.join();
}

void pulse_detector::set_frame_size(const size_t& new_frame_width, const size_t& new_frame_height) {
    assert(new_frame_width * new_frame_height <= MAX_FRAME_SIZE);
    assert(frames_to_process.empty());
    frame_width = new_frame_width;
    frame_height = new_frame_height;
}

void pulse_detector::upload_rgb_frame(shared_ptr<THREE_COLOR_FRAME>& frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height) {
    assert(new_frame_width == frame_width);
    assert(new_frame_height == frame_height);
    
    // Assuming it is RGB frame
    // And we use Green component
    auto one_color_frame_ptr = make_shared<ONE_COLOR_FRAME>((*frame_ptr.get())[1]);
    frames_to_process.push(one_color_frame_ptr);
}

void pulse_detector::instant_process_rgb_frame(shared_ptr<THREE_COLOR_FRAME>& frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height) {
    assert(new_frame_width == frame_width);
    assert(new_frame_height == frame_height);
    // Assuming it is RGB frame
    processed_frames.push_back(count_pixel_avg(frame_ptr));
}

void pulse_detector::instant_process_green_frame(shared_ptr<ONE_COLOR_FRAME>& frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height) {
    assert(new_frame_width == frame_width);
    assert(new_frame_height == frame_height);
    processed_frames.push_back(count_pixel_avg(frame_ptr.get()));
}

void pulse_detector::instant_process_yuv_frame(YUV_FRAME* frame_ptr, const size_t& new_frame_width, const size_t& new_frame_height) {
    size_t max_val = new_frame_width * new_frame_height;
    size_t u_shift = max_val;
    size_t v_shift = max_val * 1.25;
    shared_ptr<ONE_COLOR_FRAME> green_frame = make_shared<ONE_COLOR_FRAME>();
    auto rgb_frame_ptr = green_frame.get();

#pragma clang loop vectorize(enable) interleave(enable)
    for (size_t index = 0; index < max_val / 2; ++index) {
        int16_t U, V;
        U = (*frame_ptr)[u_shift + index];
        V = (*frame_ptr)[v_shift + index];

#pragma clang loop vectorize(enable) interleave(enable)
        for (size_t shift = 2 * index; shift < 2 * index + 2; ++shift) {
            int16_t Y, G;//, R, B;
            Y = (*frame_ptr)[shift];

            //Commented code for YUV -> RGB conversation
            //R = Y + 1.402 * (V - 128);
            G = Y - 0.344 * (U - 128) - 0.714 * (V - 128);
            //B = Y + 1.772 * (U - 128);

            //(*red_ptr)[index] = max(int16_t(0), min(int16_t(255), R));
            (*rgb_frame_ptr)[shift] = max(int16_t(0), min(int16_t(255), G));
            //(*blue_ptr)[index] = max(int16_t(0), min(int16_t(255), B));
        }
    }

    instant_process_green_frame(green_frame, new_frame_width, new_frame_height);
}

double pulse_detector::count_pixel_avg(ONE_COLOR_FRAME* frame_ptr) {
    size_t pixel_num = 0;
    double pixel_sum = 0;
    
    if (ENABLE_FRAME_CROP && (frame_width >= CROP_WIDTH || frame_height >= CROP_HEIGHT)) {
        size_t crop_width_real = std::max(0, int(std::min(int(frame_width), CROP_WIDTH)));
        size_t crop_height_real = std::max(0, int(std::min(int(frame_height), CROP_HEIGHT)));
        size_t left_x = std::max(0, int(frame_width / 2 - crop_width_real / 2));
        size_t right_x = std::min(frame_width, frame_width / 2 + crop_width_real / 2);
        size_t up_y = std::max(0, int(frame_height / 2 - crop_height_real / 2));
        size_t low_y = std::min(frame_height, frame_height / 2 + crop_height_real / 2);
        pixel_num = crop_width_real * crop_height_real;
        
        for (size_t y = up_y; y < low_y; ++y) {
            auto left_iter = (*frame_ptr).cbegin() + y * frame_width + left_x;
            auto right_iter = (*frame_ptr).cbegin() + y * frame_width + right_x;
            pixel_sum += std::accumulate(left_iter, right_iter, 0);
        }
    }
    else {
        pixel_num = frame_width * frame_height;
        pixel_sum = std::accumulate((*frame_ptr).cbegin(), (*frame_ptr).cbegin() + pixel_num, 0);
    }
    
    return pixel_sum / pixel_num;
}

double pulse_detector::count_pixel_avg(shared_ptr<THREE_COLOR_FRAME>& frame_ptr) {
    return count_pixel_avg(&((*frame_ptr.get())[1])); // Use Green Component
}

double pulse_detector::count_pixel_avg() {
    auto top_frame_ptr = frames_to_process.front();
    frames_to_process.pop();
    
    return count_pixel_avg(top_frame_ptr.get());
}

std::vector<double> pulse_detector::rolling_mean_window(const std::vector<double> &input, size_t window_size) {
    std::vector<double> result;
    result.reserve(input.size());

    if (input.size() < 1) {
        return result;
    }

    window_size = max(size_t(1), window_size);

    int left_index = 0;
    int right_index = 0;
    double cur_sum = input[0];
    result.push_back(cur_sum);

    while (left_index < input.size() - 1) {
        if (right_index < input.size() - 1) {
            cur_sum += input[++right_index];

            if (right_index - left_index + 1 > window_size) {
                cur_sum -= input[left_index++];
            }
        } else {
            cur_sum -= input[left_index++];
        }
        result.push_back(cur_sum / (right_index - left_index + 1));
    }

    return result;
}

std::vector<double> pulse_detector::simple_signal_normalization(const std::vector<double> &input, size_t window_size) {
    std::vector<double> signal_trend = rolling_mean_window(input, window_size);

    std::vector<double> result;
    result.reserve(input.size());
    for (size_t i = 0; i < input.size(); ++i) {
        result.push_back(input[i] - signal_trend[i]);
    }

    return result;
}

std::vector<size_t> pulse_detector::get_max_indices(const std::vector<double> &input, size_t window_size) {
    std::vector<size_t> max_indices;
    for (size_t i = 0; i < input.size(); ++i) {
        bool is_max = true;
        size_t shift_iter = 1;

        while (is_max && shift_iter < window_size) {
            size_t left_iter = max(0, int(i - shift_iter));
            size_t right_iter = min(int(input.size() - 1), int(i + shift_iter));

            if (input[i] < input[left_iter] || input[i] < input[right_iter]) {
                is_max = false;
            }

            ++shift_iter;
        }

        if (is_max) {
            max_indices.push_back(i);
        }
    }

    return max_indices;
}

std::vector<size_t>
pulse_detector::specify_max_indices(const std::vector<double> &input, const std::vector<size_t> &max_indices,
                                    size_t window_size) {
    std::vector<size_t> detailed_indices;
    detailed_indices.reserve(max_indices.size());
    // change window size
    window_size = 4;

    for (size_t i = 0; i < max_indices.size(); ++i) {
        size_t decreasing_value = 0;
        double cur_max = input[max_indices[i]];
        size_t max_index = max_indices[i];
        size_t cur_index = max(0, int(max_index - 1));

        // go left
        double left_max = cur_max;
        size_t left_max_index = max_index;
        while (decreasing_value < window_size && cur_index > detailed_indices.back()) {
            if (input[cur_index] > left_max) {
                left_max = input[cur_index];
                left_max_index = cur_index;
                decreasing_value = 0;
            }
            else {
                ++decreasing_value;
            }
            cur_index = max(0, int(cur_index - 1));
        }

        // go right
        cur_index = min(int(input.size() - 1), int(max_index + 1));
        double right_max = cur_max;
        size_t right_max_index = max_index;
        decreasing_value = 0;
        while (decreasing_value < window_size && cur_index < input.size()) {
            if (input[cur_index] > right_max) {
                right_max = input[cur_index];
                right_max_index = cur_index;
                decreasing_value = 0;
            }
            else {
                ++decreasing_value;
            }
            cur_index = min(int(input.size() - 1), int(cur_index + 1));
        }

        max_index = right_max > left_max ? right_max_index : left_max_index;
        detailed_indices.push_back(max_index);
        // just index?
    }

    return detailed_indices;
}

void pulse_detector::calculate_bpm(const std::vector<size_t> &input) {
    double window_bpm = input.size() * (60.0 / window_duration_sec); // 60 seconds in minutes
    bpm_list.push_back(window_bpm);
}

void pulse_detector::convert_and_append_to_beats(const std::vector<size_t> &input) {
    // We rely on that all time between frames is always the same = 1 / fps
    double shift = beats_list.size() == 0 ? 0 : round(beats_list[beats_list.size() - 1] / window_duration_sec) *
                                                window_duration_sec;
    std::vector<double> converted_input(input.size());

    for (size_t i = 0; i < input.size(); ++i) {
        converted_input[i] = input[i] * 1.0 / producer_fps + shift;
    }

    beats_list.reserve(beats_list.size() + converted_input.size());
    beats_list.insert(beats_list.end(), converted_input.begin(), converted_input.end());
}

void pulse_detector::calculate_hrv(const std::vector<double> &input, const bool &convert_to_ms) {
    hrv_list.clear();
    hrv_list.reserve(input.size() - 1);
    // TODO: do not recalc whole hrv each time

    for (size_t i = 0; i < input.size() - 1; ++i) {
        double rr_interval = input[i + 1] - input[i];

        if (convert_to_ms) {
            rr_interval *= 1000;
        }

        hrv_list.push_back(rr_interval);
    }
}

void pulse_detector::wait_processing_complete() {
    while (!frames_to_process.empty()) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

std::vector<double> pulse_detector::get_bpm() const {
    return bpm_list;
}

std::vector<double> pulse_detector::get_beats() const {
    return beats_list;
}

std::vector<double> pulse_detector::get_hrv() const {
    return hrv_list;
}

void pulse_detector::filter_hrv_list_quantiles(const size_t &window_size, const double &lower_quantile,
                                               const double &upper_quantile) {
    size_t left_index = 0;
    size_t right_index = min(window_size, hrv_list.size());

    while (left_index < hrv_list.size()) {
        std::vector<double> sorted_hrv(hrv_list.begin() + left_index, hrv_list.begin() + right_index);
        std::sort(sorted_hrv.begin(), sorted_hrv.end());
        size_t median_ind = min(max(int(sorted_hrv.size() / 2), 0), int(sorted_hrv.size()));
        size_t lower_limit_ind = min(max(int(sorted_hrv.size() * lower_quantile), 0), int(sorted_hrv.size()));
        size_t upper_limit_ind = max(min(int(sorted_hrv.size() * upper_quantile), int(sorted_hrv.size())), 0);

        double window_median = sorted_hrv[median_ind];
        double lower_limit = sorted_hrv[lower_limit_ind];
        double upper_limit = sorted_hrv[upper_limit_ind];

        for (size_t i = left_index; i < right_index; ++i) {
            if (hrv_list[i] < lower_limit || hrv_list[i] > upper_limit) {
                hrv_list[i] = window_median;
            }
        }

        left_index = right_index;
        right_index = min(right_index + window_size, hrv_list.size());
    }
}

void
pulse_detector::filter_hrv_list_median(const size_t &window_size, const double &lower_mul, const double &upper_mul) {
    size_t left_index = 0;
    size_t right_index = min(window_size, hrv_list.size());

    while (left_index < hrv_list.size()) {
        std::vector<double> sorted_hrv(hrv_list.begin() + left_index, hrv_list.begin() + right_index);
        std::sort(sorted_hrv.begin(), sorted_hrv.end());
        size_t median_ind = min(max(int(sorted_hrv.size() / 2), 0), int(sorted_hrv.size()));
        double window_median = sorted_hrv[median_ind];
        double lower_limit = lower_mul * window_median;
        double upper_limit = upper_mul * window_median;

        for (size_t i = left_index; i < right_index; ++i) {
            if (hrv_list[i] < lower_limit || hrv_list[i] > upper_limit) {
                hrv_list[i] = window_median;
            }
        }

        left_index = right_index;
        right_index = min(right_index + window_size, hrv_list.size());
    }
}

double pulse_detector::get_sdnn() const {
    double average = 0;

    if (hrv_list.size() < 1) {
        return average;
    }

    for (size_t i = 0; i < hrv_list.size(); ++i) {
        average += hrv_list[i];
    }

    average /= hrv_list.size();
    double sd = 0;

    for (size_t i = 0; i < hrv_list.size(); ++i) {
        sd += (hrv_list[i] - average) * (hrv_list[i] - average);
    }

    sd = sqrt(sd / hrv_list.size());
    return sd;
}

double pulse_detector::get_rmssd() const {
    double squared_diff_sum = 0;

    if (hrv_list.size() < 1) {
        return squared_diff_sum;
    }

    for (size_t i = 0; i < hrv_list.size() - 1; ++i) {
        squared_diff_sum += (hrv_list[i + 1] - hrv_list[i]) * (hrv_list[i + 1] - hrv_list[i]);
    }

    return sqrt(squared_diff_sum / max(int(hrv_list.size() - 1), 1));
}

double pulse_detector::get_stress_level() const {
    constexpr double LOWEST_RMSSD = 30;
    constexpr double HIGHEST_RMSSD = 170;
    constexpr double LOWEST_STRESS_MARK = 0;
    constexpr double HIGHEST_STRESS_MARK = 10;

    double rmssd = get_rmssd();
    double relative_stress_mark = 0;

    if (rmssd < LOWEST_RMSSD) {
        relative_stress_mark = 1;
    } else if (rmssd > HIGHEST_RMSSD) {
        relative_stress_mark = 0;
    } else {
        // y = 1 - x ^ (1/3) function
        relative_stress_mark = 1.0 - pow(((rmssd - LOWEST_RMSSD) / (HIGHEST_RMSSD - LOWEST_RMSSD)), 1.0 / 3);
    }

    return relative_stress_mark * (HIGHEST_STRESS_MARK - LOWEST_STRESS_MARK) + LOWEST_STRESS_MARK;
}

void pulse_detector::set_butterworth_freq(double low_freq, double hi_freq) {
    bpm_low = low_freq;
    bpm_high = hi_freq;
}
