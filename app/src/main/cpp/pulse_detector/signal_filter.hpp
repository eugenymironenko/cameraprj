//
// Created by Nikita Mishchanka on 2019-07-11.
//

#pragma once

#define PI 3.1415

using namespace std;

#include <vector>
#include <complex>
#include <math.h>

class signal_filter {
private:
    vector<double> TrinomialMultiply(const int& FilterOrder, const vector<double>& b, const vector<double>& c);
    vector<double> ComputeLP(const int& FilterOrder);
    vector<double> ComputeHP(const int& FilterOrder);
    vector<double> ComputeDenCoeffs(const int& FilterOrder, const double& Lcutoff, const double& Ucutoff);
    vector<double> ComputeNumCoeffs(const int& FilterOrder, const double& Lcutoff, const double& Ucutoff, const vector<double>& DenC);

    vector<double> coeff_a, coeff_b;
public:
    signal_filter(const double& low_pass_fq, const double& high_pass_fq, const double& sample_fq, const int& filter_order);
    ~signal_filter();

    vector<double> filter_signal(const vector<double>& signal);
};
