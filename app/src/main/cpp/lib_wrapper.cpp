#include <jni.h>
#include <string>
#include "pulse_detector/pulse_detector.hpp"
#include <memory>
#include <chrono>

extern "C" {
JNIEXPORT jstring JNICALL
Java_com_test_cameraprj_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */);

JNIEXPORT jstring JNICALL
Java_com_test_cameraprj_MainActivity_myStringFromCpp(
        JNIEnv *env,
        jobject obj);

JNIEXPORT void JNICALL
Java_com_test_cameraprj_MainActivity_initDetector(
        JNIEnv *env,
        jobject obj,
        jint video_fps,
        jint frame_width,
        jint frame_height);

JNIEXPORT jdouble JNICALL
Java_com_test_cameraprj_MainActivity_testDetectorPerf(
        JNIEnv *env,
        jobject obj,
        jint frames_num,
        jint frame_width,
        jint frame_height);

JNIEXPORT void JNICALL
Java_com_test_cameraprj_MainActivity_uploadFrameToDetector(
        JNIEnv *env,
        jobject obj,
        jobject y_buffer,
        jobject u_buffer,
        jobject v_buffer,
        jint frame_width,
        jint frame_height);

JNIEXPORT jdouble JNICALL
Java_com_test_cameraprj_MainActivity_getLastBpm(
        JNIEnv *env,
        jobject obj);
}

std::shared_ptr<pulse_detector> detector = nullptr;

std::shared_ptr<YUV_FRAME>
get_frame_from_buffer(JNIEnv *pEnv, jobject *yBuffer, jobject *uBuffer, jobject *vBuffer) {
    void *yData = pEnv->GetDirectBufferAddress(*yBuffer);
    void *uData = pEnv->GetDirectBufferAddress(*uBuffer);
    void *vData = pEnv->GetDirectBufferAddress(*vBuffer);
    uint32_t ySize = static_cast<uint32_t >(pEnv->GetDirectBufferCapacity(*yBuffer));
    uint32_t uSize = static_cast<uint32_t >(pEnv->GetDirectBufferCapacity(*uBuffer));
    uint32_t vSize = static_cast<uint32_t >(pEnv->GetDirectBufferCapacity(*vBuffer));
    uint32_t frameSize = ySize + uSize + vSize;

    auto y_plane_ptr = static_cast<const uint8_t *>(yData);
    auto u_plane_ptr = static_cast<const uint8_t *>(uData);
    auto v_plane_ptr = static_cast<const uint8_t *>(vData);
    std::shared_ptr<YUV_FRAME> yuv_frame_ptr = make_shared<YUV_FRAME>();

    //memcpy(yuv_frame_ptr->data(), y_plane_ptr, ySize);

    copy(y_plane_ptr, y_plane_ptr + ySize, yuv_frame_ptr->data());
    copy(u_plane_ptr, u_plane_ptr + uSize, yuv_frame_ptr->data() + ySize);
    copy(v_plane_ptr, v_plane_ptr + vSize, yuv_frame_ptr->data() + ySize + uSize);

    return yuv_frame_ptr;
}

JNIEXPORT jstring JNICALL
Java_com_test_cameraprj_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

JNIEXPORT jstring JNICALL
Java_com_test_cameraprj_MainActivity_myStringFromCpp(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello;
    if (detector == nullptr) {
        hello = "C++: detector is nullptr";
    } else {
        hello = "C++: detector is not nullptr";
    }

    return env->NewStringUTF(hello.c_str());
}

JNIEXPORT void JNICALL
Java_com_test_cameraprj_MainActivity_initDetector(
        JNIEnv *env,
        jobject obj,
        jint video_fps,
        jint frame_width,
        jint frame_height) {
    if (detector == nullptr) {
        detector = std::make_shared<pulse_detector>(video_fps);
        // frame_width and frame_height should be positive!

        detector->set_frame_size(size_t(frame_width), size_t(frame_height));
    }
}

JNIEXPORT jdouble JNICALL
Java_com_test_cameraprj_MainActivity_testDetectorPerf(
        JNIEnv *env,
        jobject obj,
        jint frames_num,
        jint frame_width,
        jint frame_height) {
    pulse_detector test_detector(30);
    test_detector.set_frame_size(size_t(frame_width), size_t(frame_height));

    double milliseconds_passed = 0;
    std::chrono::steady_clock::time_point begin, end;
    // process frames

    for (int i = 0; i < frames_num; ++i) {
        YUV_FRAME test_frame;
        for (int j = 0; j < frame_width * frame_height; ++j) {
            test_frame[j] = rand() % 254;
        }

        begin = std::chrono::steady_clock::now();
        test_detector.instant_process_yuv_frame(&test_frame, frame_width, frame_height);
        end = std::chrono::steady_clock::now();
        milliseconds_passed += std::chrono::duration_cast<std::chrono::milliseconds>(
                end - begin).count();
    }

    double fps = frames_num / (milliseconds_passed / 1000.f);
    return fps;
}

JNIEXPORT void JNICALL
Java_com_test_cameraprj_MainActivity_uploadFrameToDetector(
        JNIEnv *env,
        jobject obj,
        jobject y_buffer,
        jobject u_buffer,
        jobject v_buffer,
        jint frame_width,
        jint frame_height) {
    auto yuv_frame_ptr = get_frame_from_buffer(env, &y_buffer, &u_buffer, &v_buffer);
    detector->instant_process_yuv_frame(yuv_frame_ptr.get(), frame_width, frame_height);
}

JNIEXPORT jdouble JNICALL
Java_com_test_cameraprj_MainActivity_getLastBpm(
        JNIEnv *env,
        jobject obj) {
    auto bpm_list = detector->get_bpm();
    if (bpm_list.size() > 0) {
        return bpm_list.back();
    }
    else {
        return -1;
    }
}

// jdoubleArray output = env->NewDoubleArray( results.size() );
//env->SetDoubleArrayRegion( output, 0, results.size(), &results[0] );
